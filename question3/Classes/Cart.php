<?php

namespace Classes;

use Classes\Customer;

/**
 * Class Cart
 * 
 * Represents a shopping cart with a customer, a collection of items, and functionalities
 * to calculate costs including tax and shipping.
 */
class Cart
{
    private $customer;
    private $items = [];
    private $taxRate = 0.07;

    /**
     * Constructor for the Cart class.
     *
     * @param Customer $customer The customer associated with this cart.
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Adds an item to the cart.
     *
     * @param mixed $item The item to add to the cart.
     */
    public function addItem($item)
    {
        $this->items[] = $item;
    }

    /**
     * Calculates the subtotal of all items in the cart.
     *
     * @return float The subtotal of the items in the cart.
     */
    public function calculateSubtotal()
    {
        $subtotal = 0;
        foreach ($this->items as $item) {
            $subtotal += $item->getCost();
        }
        return $subtotal;
    }

    /**
     * Calculates the tax for the given subtotal.
     *
     * @param float $subtotal The subtotal for which the tax is to be calculated.
     * @return float The calculated tax amount.
     */
    public function calculateTax($subtotal)
    {
        return $subtotal * $this->taxRate;
    }

    /**
     * Calculates the shipping cost.
     * Assumes connection to a shipping rate API or a fixed cost.
     *
     * @return float The calculated shipping cost.
     */
    public function calculateShippingCost()
    {
        return 10.00; // Placeholder for shipping cost calculation.
    }

    /**
     * Calculates the total cost including subtotal, tax, and shipping.
     *
     * @return float The total cost of the items in the cart.
     */
    public function calculateTotal()
    {
        $subtotal = $this->calculateSubtotal();
        $tax = $this->calculateTax($subtotal);
        $shipping = $this->calculateShippingCost();
        return $subtotal + $tax + $shipping;
    }

    /**
     * Gets the name of the customer associated with the cart.
     *
     * @return string The name of the customer.
     */
    public function getCustomerName()
    {
        return $this->customer->getName();
    }

    /**
     * Retrieves the addresses of the customer associated with the cart.
     *
     * @return array The addresses of the customer.
     */
    public function getCustomerAddresses()
    {
        return $this->customer->getAddresses();
    }

    /**
     * Gets all items currently in the cart.
     *
     * @return array An array of items in the cart.
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Determines where the order will be shipped.
     *
     * @return mixed The main shipping address of the customer.
     */
    public function whereOrderShips()
    {
        return $this->customer->getMainAddress();
    }

    /**
     * Calculates the cost of each item in the cart, including shipping and tax.
     *
     * @return array An array of items with their respective costs.
     */
    public function costOfItemInCart()
    {
        return array_map(function ($item) {
            $item->cost = $item->getPrice() + $this->calculateShippingCost() + ($item->getPrice() * $this->taxRate);
            return $item;
        }, $this->items);
    }
}
