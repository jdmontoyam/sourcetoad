<?php

namespace Classes;

/**
 * Class Item
 * 
 * Represents an item with an ID, name, quantity, and price.
 */
class Item
{
    private $id;
    private $name;
    private $quantity;
    private $price;

    /**
     * Constructor for the Item class.
     *
     * @param int $id The ID of the item.
     * @param string $name The name of the item.
     * @param int $quantity The quantity of the item.
     * @param float $price The price of one unit of the item.
     */
    public function __construct(int $id, string $name, int $quantity, float $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->quantity = $quantity;
        $this->price = $price;
    }

    /**
     * Calculates and returns the total cost of the item based on its quantity and price.
     *
     * @return float The total cost of the item.
     */
    public function getCost()
    {
        return $this->quantity * $this->price;
    }

    /**
     * Retrieves the price of one unit of the item.
     *
     * @return float The price of the item.
     */
    public function getPrice()
    {
        return $this->price;
    }
}
