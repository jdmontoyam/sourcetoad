<?php

namespace Classes;

class Address
{
    private $line_1;
    private $line_2;
    private $city;
    private $state;
    private $zip;

    /**
     * Address class constructor.
     *
     * @param string $line_1 First line of the address.
     * @param string $line_2 Second line of the address (optional).
     * @param string $city City.
     * @param string $state State or province.
     * @param string $zip Postal code.
     */
    public function __construct(string $line_1, string $line_2, string $city, string $state, string $zip)
    {
        $this->line_1 = $line_1;
        $this->line_2 = $line_2;
        $this->city = $city;
        $this->state = $state;
        $this->zip = $zip;
    }

    // Getters

    /**
     * Gets the first line of the address.
     *
     * @return string First line of the address.
     */
    public function getLine1()
    {
        return $this->line_1;
    }

    /**
     * Gets the second line of the address.
     *
     * @return string Second line of the address.
     */
    public function getLine2()
    {
        return $this->line_2;
    }

    /**
     * Gets the city of the address.
     *
     * @return string City of the address.
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Gets the state or province of the address.
     *
     * @return string State or province of the address.
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Gets the postal code of the address.
     *
     * @return string Postal code of the address.
     */
    public function getZip()
    {
        return $this->zip;
    }
}
