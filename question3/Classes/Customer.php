<?php

namespace Classes;

/**
 * Class Customer
 * 
 * Represents a customer with a first name, last name, and a list of addresses.
 */
class Customer
{
    private $first_name;
    private $last_name;
    private $addresses;

    /**
     * Constructor for the Customer class.
     *
     * @param string $first_name The first name of the customer.
     * @param string $last_name The last name of the customer.
     * @param array $addresses An array of addresses associated with the customer.
     */
    public function __construct(string $first_name, string $last_name, array $addresses)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->addresses = $addresses;
    }

    /**
     * Adds an address to the customer's list of addresses.
     *
     * @param mixed $address The address to add. Should be of type Address.
     */
    public function addAddress($address)
    {
        $this->addresses[] = $address;
    }

    /**
     * Gets the full name of the customer.
     *
     * @return string Returns the full name of the customer.
     */
    public function getName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Retrieves all addresses of the customer.
     *
     * @return array An array of the customer's addresses.
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Gets the main (first) address of the customer.
     *
     * @return mixed Returns the main address if exists, otherwise a string indicating no address is registered.
     */
    public function getMainAddress()
    {
        return isset($this->addresses[0]) ? $this->addresses[0] : 'No address registered';
    }
}
