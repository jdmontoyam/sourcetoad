<?php

require_once 'Classes/Cart.php';
require_once 'Classes/Item.php';
require_once 'Classes/Address.php';
require_once 'Classes/Customer.php';

use Classes\{Cart, Item, Address, Customer};

function printSection($title, $data)
{
    echo "<h3>$title</h3>";
    print_r($data);
}

$address_1 = new Address('example line 1', 'example line 2', 'Medellin', 'Antioquia', '055450');
$address_2 = new Address('example line 1', 'example line 2', 'Manizales', 'Caldas', '170001');

$customer = new Customer('David', 'Montoya', [$address_1, $address_2]);

$item_1 = new Item('1', 'Item 1', 1, 100);
$item_2 = new Item('2', 'Item 2', 2, 200);
$item_3 = new Item('3', 'Item 3', 3, 300);


$cart = new Cart($customer);
$cart->addItem($item_1);
$cart->addItem($item_2);
$cart->addItem($item_3);

printSection('Customer Name', $cart->getCustomerName());
printSection('Customer Addresses', $cart->getCustomerAddresses());
printSection('Items in Cart', $cart->getItems());
printSection('Cost of Items in Cart', $cart->costOfItemInCart());
printSection('Where Order Ships', $cart->whereOrderShips());
printSection('Subtotal for all items', $cart->calculateSubtotal());
printSection('Total for all items', $cart->calculateTotal());
