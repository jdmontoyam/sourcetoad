<?php

$data = [
    [
        'guest_id' => 177,
        'guest_type' => 'crew',
        'first_name' => 'Marco',
        'middle_name' => null,
        'last_name' => 'Burns',
        'gender' => 'M',
        'guest_booking' => [
            [
                'booking_number' => 20008683,
                'ship_code' => 'OST',
                'room_no' => 'A0073',
                'start_time' => 1438214400,
                'end_time' => 1483142400,
                'is_checked_in' => true,
            ],
        ],
        'guest_account' => [
            [
                'account_id' => 20009503,
                'status_id' => 2,
                'account_limit' => 0,
                'allow_charges' => true,
            ],
        ],
    ],
    [
        'guest_id' => 10000113,
        'guest_type' => 'crew',
        'first_name' => 'Bob Jr ',
        'middle_name' => 'Charles',
        'last_name' => 'Hemingway',
        'gender' => 'M',
        'guest_booking' => [
            [
                'booking_number' => 10000013,
                'room_no' => 'B0092',
                'is_checked_in' => true,
            ],
        ],
        'guest_account' => [
            [
                'account_id' => 10000522,
                'account_limit' => 300,
                'allow_charges' => true,
            ],
        ],
    ],
    [
        'guest_id' => 10000114,
        'guest_type' => 'crew',
        'first_name' => 'Al ',
        'middle_name' => 'Bert',
        'last_name' => 'Santiago',
        'gender' => 'M',
        'guest_booking' => [
            [
                'booking_number' => 10000014,
                'room_no' => 'A0018',
                'is_checked_in' => true,
            ],
        ],
        'guest_account' => [
            [
                'account_id' => 10000013,
                'account_limit' => 300,
                'allow_charges' => false,
            ],
        ],
    ],
    [
        'guest_id' => 10000115,
        'guest_type' => 'crew',
        'first_name' => 'Red ',
        'middle_name' => 'Ruby',
        'last_name' => 'Flowers ',
        'gender' => 'F',
        'guest_booking' => [
            [
                'booking_number' => 10000015,
                'room_no' => 'A0051',
                'is_checked_in' => true,
            ],
        ],
        'guest_account' => [
            [
                'account_id' => 10000519,
                'account_limit' => 300,
                'allow_charges' => true,
            ],
        ],
    ],
    [
        'guest_id' => 10000116,
        'guest_type' => 'crew',
        'first_name' => 'Ismael ',
        'middle_name' => 'Jean-Vital',
        'last_name' => 'Jammes',
        'gender' => 'M',
        'guest_booking' => [
            [
                'booking_number' => 10000016,
                'room_no' => 'A0023',
                'is_checked_in' => true,
            ],
        ],
        'guest_account' => [
            [
                'account_id' => 10000015,
                'account_limit' => 300,
                'allow_charges' => true,
            ],
        ],
    ],
];

function print_nested($data, $indent = 0)
{
    foreach ($data as $key => $value) {
        echo str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;", $indent) . $key . ': ';

        if (is_array($value)) {
            echo "<br>";
            print_nested($value, $indent + 1);
        } else {
            echo $value . "<br>";
        }
    }
}

echo "<h2 style='color:red; text-align:center'> Question 1 </h2>";
print_nested($data);

echo "<h2 style='color:red; text-align:center'> Question 2 </h2>";

function searchValue($array, $key)
{
    if (isset($array[$key])) {
        return $array[$key];
    } else {
        foreach ($array as $subArray) {
            if (is_array($subArray)) {
                $result = searchValue($subArray, $key);
                if ($result !== null) {
                    return $result;
                }
            }
        }
    }

    return null;
}


function sortMultiLevelArray(&$array, $sortKeys)
{
    if (!is_array($array)) {
        return;
    }

    usort($array, function ($a, $b) use ($sortKeys) {
        foreach ($sortKeys as $key) {
            $valueA = searchValue($a, $key);
            $valueB = searchValue($b, $key);

            if ($valueA != $valueB) {
                return $valueA <=> $valueB;
            }
        }
        return 0;
    });
}

sortMultiLevelArray($data, ['last_name', 'account_id']);

echo "<pre>";
print_r($data);
echo "</pre>";
